## Exercice 1.2.5 : Explorer un arbre (JavaScript)

Dans le langage de programmation de votre choix : 

* Ecrire la méthode qui permet d'ajouter un nœud dans un arbre binaire de recherche

* Définir la méthode pour trouver une valeur donnée dans un arbre binaire de recherche

* Écrire la méthode pour afficher l’arbre selon un parcours infixe

* [optionnel] Écrire la méthode pour supprimer un nœud donné en distinguant trois cas :
    * Le nœud est une feuille -> suppression simple

    * Le nœud a un seul enfant -> il est remplacé par lui

    * Le nœud à deux enfants, on le remplace alors par le nœud le plus proche, c’est à dire le nœud le plus à droite de l’arbre gauche ou le plus à gauche de l'arbre droit.
