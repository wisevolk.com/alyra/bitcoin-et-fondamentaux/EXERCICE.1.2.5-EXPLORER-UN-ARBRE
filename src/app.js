// const chalk = require("chalk");

const chalk = require("chalk");

class Node {
    constructor(val) {
        this.val = val;
        this.left = undefined;
        this.right = undefined;
        this.parent = undefined;
    }

    deleteChild(node) {
        if ( this.left === node )
            this.left = undefined;
        else
            this.right = undefined;
    }
}

class Tree {
    constructor() {
        this.racine = undefined;
        this.infixTab = [];
    }

    addNode(val, nd = this.racine) {
        try {
            if ( this.racine === undefined ) {
                this.racine = new Node(val);
                // console.log(this.racine.val)
                return;
            }
            if ( val < nd.val ) {
                if ( nd.left !== undefined ) {
                    this.addNode(val, nd.left);
                } else {
                    nd.left = new Node(val);
                    nd.left.parent = nd;
                    // console.log(nd.left.val)
                    return;
                }
            } else {
                if ( nd.right !== undefined ) {
                    this.addNode(val, nd.right);
                } else {
                    nd.right = new Node(val);
                    nd.right.parent = nd;
                    // console.log(nd.right.val)
                    return;
                }
            }
        } catch (e) {
            console.log(chalk.red(e));
        }
    }

    displayInfix(nd = this.racine) {
        if ( nd.left !== undefined ) {
            this.displayInfix(nd.left);
        }
        this.infixTab.push(nd.val);
        if ( nd.right !== undefined ) {
            this.displayInfix(nd.right);
        }
        return this.infixTab;
    }

    findNode(val, nd = this.racine) {
        if ( nd.val === val ) {
            return nd;
        } else if ( val < nd.val && nd.left !== undefined ) {
            return this.findNode(val, nd.left);
        } else if ( val > nd.val && nd.right !== undefined ) {
            return this.findNode(val, nd.right);
        }
        return undefined;
    }

    //TODO deleteNode() ajouté les cas 1 enfant et 2 enfants
    deleteNode(val) {
        let node = this.findNode(val);
        try {
            if ( node === undefined ) throw  new Error(`Le noeud ${ val } n'existe pas !`)
            // this.infixTab[node.position] = undefined;
            switch (true) {
                // Le noeud est une feuille --> Suppression simple
                case (node.left === undefined) && (node.right === undefined):
                    node.parent.deleteChild(node);
                    console.log(chalk.magenta( `Feuille (noeud) ${node.val} supprimée`));
                    node = undefined;
                    break;
                // Le noeud à un seul enfant --> Il est remplacé par lui
                case (node.left !== undefined) || (node.right !== undefined):
                    console.log(chalk.magenta( "1 enfant"));
                    // let parent = node.parent;
                    // node = (node.left !== undefined) ? node.left : node.right;
                    //
                    // node.parent = parent;
                    break;
                // Le noeud à 2 enfants
                // --> Si le noeud est à gauche de l'arbre on le remplace par son enfant de droite.
                // --> Si le noeud est à droite de l'arbre on le remplace par son enfant de gauche.
                case(node.left !== undefined) && (node.right !== undefined):
                    console.log(chalk.magenta( "2 enfants"));
                    break;
                default:
            }
            return true;
        } catch (err) {
            console.log(err.message);
        }
    }

    toString() {
        return this.infixTab.toString();
    }
}


console.clear();
const a = new Tree();
a.addNode(30);
a.addNode(18);
a.addNode(24);
a.addNode(11);
a.addNode(33);
a.addNode(13);
a.addNode(40);
a.addNode(46);
a.addNode(14);
a.addNode(21);
a.addNode(12);
a.addNode(10);
a.addNode(31);
a.addNode(35);
a.addNode(32);

console.log(a.findNode(30));
console.log(a.findNode(32));
console.log(a.findNode(11));
console.log(a.findNode(24));


console.log(a.deleteNode(14));
console.log(chalk.cyan(a.displayInfix()));


